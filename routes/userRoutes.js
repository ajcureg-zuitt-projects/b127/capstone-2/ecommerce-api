const express = require('express');
const router = express.Router();
const userControllers = require('../controllers/userControllers')
const auth = require('../auth');


router.post('/register', (req,res)=>{
	userControllers.register(req.body).then(result => res.send(result));
})


router.post('/register/admin', (req,res)=>{
	req.body.isAdmin = true;
	userControllers.register(req.body).then(result => res.send(result));
})


router.post('/checkEmail', (req, res) =>{
	userControllers.checkEmailExist(req.body).then(result => res.send(result));
})


router.post('/checkUsername', (req, res) =>{
	userControllers.checkUsernameExist(req.body).then(result => res.send(result));
})


router.put('/makeAdmin', auth.verify, (req,res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		userControllers.makeUserAdmin(req.body).then(result => res.send(result))
	}else {
		res.send(false)
	}
})


router.post('/login', (req,res) => {
	userControllers.loginUser(req.body).then(result => res.send(result))
})


router.get('/checkDetail', auth.verify, (req,res) => {
	 
	const userData =  auth.decode(req.headers.authorization)
	
	userControllers.userProfile({userId:userData.id}).then(result => res.send(result));
})


module.exports = router;