const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		require: [true, 'First name is required']
	},
	lastName: {
		type: String,
		require: [true, 'Last name is required']
	},
	mobileNumber: {
		type: String,
		require: [true, 'Mobile number is required']
	},
	email: {
		type: String,
		require: [true, 'Email is required']
	},
	userName: {
		type: String,
		require: [true, 'Username is required']
	},
	password: {
		type: String,
		require: [true, 'Password is required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			brandName: {
				type: String,
				require: [true, 'Brand Name is required']
			},
			productName : {
				type: String,
				required: [true, "Product Name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Product Quantity is required"],
				default: 1
			},
			orderedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema)