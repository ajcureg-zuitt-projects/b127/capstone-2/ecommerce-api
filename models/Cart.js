const mongoose = require('mongoose');

const cartSchema = new mongoose.Schema ({
	userId:{
		type: String,
		required: [true, "User ID is required"]
	},
	totalPrice: {
		type: Number
	},
	products: [
		{
			productId : {
				type: String,
				required: [true, "Product ID is required"]
			},
			brandName: {
				type: String,
				require: [true, 'Brand Name is required']
			},
			productName : {
				type: String,
				required: [true, "Product Name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Product Quantity is required"],
				default: 1
			},
			totalPerProduct: {
				type: Number
			},
			addedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("Cart", cartSchema);